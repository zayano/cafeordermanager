package com.example.administrator.cafeom;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.administrator.cafeom.Helper.DBHelperCart;
import com.example.administrator.cafeom.ListMenuFood.ShowCartOrder;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ConfirmCost extends AppCompatActivity {

    TextView food_confirm,cost_confirm,cost_total;
    TextView quantity;
    Button order;
    Button dec_button, inc_button;
    String nameorder,costorder,quantityorder,totalcostorder;
    private int counter;
    private double total;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_cost);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Bundle bundle = getIntent().getExtras();
        food_confirm = findViewById(R.id.food_order);
        cost_confirm = findViewById(R.id.food_cost);
        cost_total = findViewById(R.id.cost_text_view);
        quantity = findViewById(R.id.quantity_text_view);
        inc_button = findViewById(R.id.increment_button);
        dec_button = findViewById(R.id.decrement_button);
        inc_button.setOnClickListener(clickListener);
        dec_button.setOnClickListener(clickListener);
        order = findViewById(R.id.btn_order);

        Intent intent = getIntent();
        if(intent != null){

            nameorder = intent.getStringExtra("food");
            costorder = intent.getStringExtra("cost");

        }

        food_confirm.setText(nameorder);
        cost_confirm.setText(costorder);

        initCounter();


        order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String order_id = generateOrderID();
                String name = food_confirm.getText().toString();
                String cost = cost_confirm.getText().toString();
                String qty = quantity.getText().toString();



                Double toyal_pay = total;
                String total1 = Double.toString(toyal_pay);


                DBHelperCart dbHelper = new DBHelperCart(getApplicationContext());
                SQLiteDatabase database = dbHelper.getWritableDatabase();
                dbHelper.insertDataOrders(order_id,name,cost,qty,total1,database);
                Toast.makeText(getApplication(), "Order Successfully Completed", Toast.LENGTH_SHORT).show();
                dbHelper.close();
            }
        });

    }

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            switch (view.getId()){

                case R.id.increment_button:

                    incrementValue();
                    break;

                case R.id.decrement_button:

                    decrementValue();
                    break;
            }

        }
    };

    private void initCounter(){

        counter = 0;
        quantity.setText(counter + "");
    }

    private void incrementValue(){

        counter++;
        quantity.setText(Integer.toString(counter));
        total = calcTotal(cost_confirm.getText().toString(),quantity.getText().toString());

        cost_total.setText("Rp."+Double.toString(total));
    }

    private void decrementValue(){

        counter--;
        quantity.setText(Integer.toString(counter));

        total = calcTotal(cost_confirm.getText().toString(),quantity.getText().toString());

        cost_total.setText("Rp."+Double.toString(total));
    }

    private double calcTotal(String qty,String price){

        qty = quantity.getText().toString();
        int quantity = Integer.parseInt(qty);

        price = cost_confirm.getText().toString();
        double priceItem = Double.parseDouble(price);


        double total = (quantity * priceItem);

        return total;


    }

    public String generateOrderID(){

        DBHelperCart dbHelper = new DBHelperCart(getApplicationContext());
        SQLiteDatabase database = dbHelper.getWritableDatabase();

        ArrayList<String> idList = new ArrayList<>();
        idList = dbHelper.getOrderIDs(database);

        String id;
        int next = idList.size();
        next++;
        id = "O10" + next;
        if (idList.contains(id)) {
            next++;
            id = "O100" + next;
        }

        return id;

    }

    public boolean onCreateOptionsMenu (Menu menu){
        DBHelperCart dbHelperCart = new DBHelperCart(this);

        getMenuInflater().inflate(R.menu.menu_shopcart, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.shop_cart_top){

//            Toast.makeText(this,"ComingSoon",Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(ConfirmCost.this,ShowCartOrder.class);
            startActivity(intent);
        }else {
            onBackPressed();
        }
        return true;
    }

}
