package com.example.administrator.cafeom.Helper;

import com.example.administrator.cafeom.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CategoryJSONParser {
    // Receives a JSONObject and returns a list
    public List<HashMap<String,Object>> parse(JSONObject jObject){

        JSONArray jCategorys = null;
        try {
            // Retrieves all the elements in the 'countries' array
            jCategorys = jObject.getJSONArray("data");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Invoking getCountries with the array of json object
        // where each json object represent a country
        return getCategorys(jCategorys);
    }

    private List<HashMap<String, Object>> getCategorys(JSONArray jCategorys){
        int categoryCount = jCategorys.length();
        List<HashMap<String, Object>> categoryList = new ArrayList<HashMap<String,Object>>();
        HashMap<String, Object> category = null;

        // Taking each country, parses and adds to list object
        for(int i=0; i<categoryCount;i++){
            try {
                // Call getCountry with country JSON object to parse the country
                category = getCategory((JSONObject)jCategorys.get(i));
                categoryList.add(category);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return categoryList;
    }

    // Parsing the Country JSON object
    private HashMap<String, Object> getCategory(JSONObject jCategory){

        HashMap<String, Object> category = new HashMap<String, Object>();
        String id = "";
        String title="";
        String description = "";
        String picture = "";

        try {
            id = jCategory.getString("id");
            title = jCategory.getString("title");
            description = jCategory.getString("description");
            picture = jCategory.getString("picture");


//            String details =        "Language : " + language + "\n" +
//                    "Capital : " + capital + "\n" +
//                    "Currency : " + currencyName + "(" + currencyCode + ")";

            category.put("id", id);
            category.put("title", title);
            category.put("description", description);
            category.put("picture", R.drawable.wedang);
            category.put("picture_path", picture);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return category;
    }
}
