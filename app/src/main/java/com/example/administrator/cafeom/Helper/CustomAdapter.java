package com.example.administrator.cafeom.Helper;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.administrator.cafeom.ListMenuFood.ShowCartOrder;
import com.example.administrator.cafeom.OrderModel;
import com.example.administrator.cafeom.R;

import java.util.ArrayList;

public class CustomAdapter extends BaseAdapter {

    private Activity activity;
    private ArrayList<ItemsModel> item;
    private LayoutInflater inflater;

    ItemsModel orderModel;

    TextView name,cost,quantity,cost_total;


    public CustomAdapter(Activity activity, ArrayList<ItemsModel> item) {
        this.activity = activity;
        this.item = item;
    }

    @Override
    public int getCount() {
        return item.size();
    }

    @Override
    public ItemsModel getItem(int position) {

        return item.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(inflater == null){

            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if(convertView == null){

            convertView = inflater.inflate(R.layout.order_chart,null);
        }

        name = convertView.findViewById(R.id.itemNames);
        cost = convertView.findViewById(R.id.prices_item);
        quantity = convertView.findViewById(R.id.quality);
        cost_total = convertView.findViewById(R.id.cost_total);

        orderModel = item.get(position);
        name.setText(orderModel.getNameOrder());
        cost.setText(orderModel.getCostOrder());
        quantity.setText(orderModel.getQuantity());
        cost_total.setText("Rp. "+orderModel.getTotal());

        return convertView;
    }
}
