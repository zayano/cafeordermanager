package com.example.administrator.cafeom.Helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.administrator.cafeom.OrderModel;

import java.util.ArrayList;

public class DBHelperCart extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "cartorder";
    public static final int DATABASE_VERSION = 1;

    public static final String TABLE_NAME = "orders";
    public static final String T2COL1 = "orderID";
    public static final String T2COL2 = "name";
    public static final String T2COL3 = "cost";
    public static final String T2COL4 = "quantity";
    public static final String T2COL5 = "total_payment";


    public static final String CREATE_TABLE_ORDERS = "CREATE TABLE " + TABLE_NAME +
            "(" + T2COL1 + " text primary key, "
            + T2COL2 + " text, "
            + T2COL3 + " text, "
            + T2COL4 + " text, "
            + T2COL5 + " text);";


    public DBHelperCart(Context context) {

        super(context, DATABASE_NAME, null, 1);
        Log.e("DATABASE OPERATIONS", "Database Created");
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE_ORDERS);

        Log.e("DATABASE OPERATIONS", "Table Created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(sqLiteDatabase);
        onCreate(sqLiteDatabase);

    }


    public void insertDataOrders(String orderCode, String name, String cost, String qty, String total, SQLiteDatabase database) {

        ContentValues contentValues = new ContentValues();
        contentValues.put(T2COL1, orderCode);
        contentValues.put(T2COL2, name);
        contentValues.put(T2COL3, cost);
        contentValues.put(T2COL4, qty);
        contentValues.put(T2COL5, total);


        long result = database.insert(TABLE_NAME, null, contentValues);
        Log.e("DATABASE OPERATIONS", "Order Inserted");

    }

    public ArrayList<OrderModel> getAllOrders() {

        ArrayList<OrderModel> list = new ArrayList<>();
        String sql = "SELECT * FROM orders";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql, null);

        if (cursor.moveToFirst()) {

            do {

                OrderModel orderModel = new OrderModel();
                orderModel.setOrderID(cursor.getString(0));
                orderModel.setNameOrder(cursor.getString(1));
                orderModel.setCostOrder(cursor.getString(2));
                orderModel.setQuantity(cursor.getString(3));
                orderModel.setTotal(cursor.getString(4));
                list.add(orderModel);

            } while (cursor.moveToNext());
        }

        return list;
    }

    public ArrayList<ItemsModel> getAllItemsCustomers(){

        ArrayList<ItemsModel> list = new ArrayList<>();
        String sql = "select * from " + TABLE_NAME;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql, null);

        if(cursor.moveToFirst()){

            do {

                ItemsModel itemsModel = new ItemsModel();
                itemsModel.setOrderID(cursor.getString(0));
                itemsModel.setNameOrder(cursor.getString(1));
                itemsModel.setCostOrder(cursor.getString(2));
                itemsModel.setQuantity(cursor.getString(3));
                itemsModel.setTotal(cursor.getString(4));
                list.add(itemsModel);

            }while (cursor.moveToNext());
        }

        return list;
    }

    public void deleteOrder(String code) {

        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, "orderID = ?", new String[]{code});
        db.close();

    }



    public ArrayList<String> getOrderIDs(SQLiteDatabase database) {

        ArrayList<String> idList = new ArrayList<>();

        Cursor cursor = database.rawQuery("SELECT orderID FROM orders", null);

        while (cursor.moveToNext()) {
            idList.add(cursor.getString(cursor.getColumnIndex(T2COL1)));
        }

        return idList;
    }
}