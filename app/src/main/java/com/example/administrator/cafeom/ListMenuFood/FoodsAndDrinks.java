package com.example.administrator.cafeom.ListMenuFood;

public class FoodsAndDrinks {
    private String name,cost;
    private int image;

    public FoodsAndDrinks(String name, String cost, int image){
        this.name = name;
        this.cost = cost;
        this.image = image;
    }

    public String getName(){
        return name;
    }

    public  String getCost(){
        return cost;
    }

    public int getImage(){
        return image;
    }
}
