package com.example.administrator.cafeom.ListMenuFood;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.dexafree.materialList.card.Card;
import com.dexafree.materialList.card.CardProvider;
import com.dexafree.materialList.card.OnActionClickListener;
import com.dexafree.materialList.card.action.TextViewAction;
import com.dexafree.materialList.view.MaterialListView;
import com.example.administrator.cafeom.ConfirmCost;
import com.example.administrator.cafeom.R;
import com.squareup.picasso.RequestCreator;

public class ListMenu extends AppCompatActivity {

    MaterialListView materialListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        this.initializeViews();
        this.bindData();

    }

    private void initializeViews(){
        materialListView = findViewById(R.id.material_menu);
    }

    private void bindData(){
        for (FoodsAndDrinks foodsAndDrinks : getData()){
            this.createCard(foodsAndDrinks);
        }
    }

    private void createCard(final FoodsAndDrinks foodsAndDrinks){
        Card card = new Card.Builder(this)
                .withProvider(new CardProvider())
                .setLayout(R.layout.material_basic_image_buttons_card_layout)
                .setTitle(foodsAndDrinks.getName())
                .setTitleGravity(Gravity.END)
                .setDescription(foodsAndDrinks.getCost())
                .setDescriptionGravity(Gravity.END)
                .setDrawable(foodsAndDrinks.getImage())
                .setDrawableConfiguration(new CardProvider.OnImageConfigListener() {
                    @Override
                    public void onImageConfigure(@NonNull RequestCreator requestCreator) {
                        requestCreator.resize(121,121);
                    }
                })
                .addAction(R.id.right_text_button, new TextViewAction(this)
                .setText("Pesan")
                .setTextResourceColor(R.color.colorAccent)
                .setListener(new OnActionClickListener() {
                    @Override
                    public void onActionClicked(View view, Card card) {
                        Intent intent = new Intent(ListMenu.this,ConfirmCost.class);
                        intent.putExtra("food",foodsAndDrinks.getName());
                        intent.putExtra("cost",foodsAndDrinks.getCost());
                        startActivity(intent);
                    }
                }))
                .endConfig()
                .build();

        materialListView.getAdapter().add(card);
    }

    private FoodsAndDrinks[] getData(){
        FoodsAndDrinks[] foodsAndDrinks1 = new FoodsAndDrinks[3];

        FoodsAndDrinks foodsAndDrinks = new FoodsAndDrinks("Pancong Special",
                "15000",
                R.drawable.pancong);
        foodsAndDrinks1[0]=foodsAndDrinks;

        foodsAndDrinks = new FoodsAndDrinks("Es Pisang Ijo",
                "10000",
                R.drawable.pisang_ijo);
        foodsAndDrinks1[1]=foodsAndDrinks;

        foodsAndDrinks = new FoodsAndDrinks("Wedang Jahe",
                "9000",
                R.drawable.wedang);
        foodsAndDrinks1[2]=foodsAndDrinks;

        return foodsAndDrinks1;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }
}
