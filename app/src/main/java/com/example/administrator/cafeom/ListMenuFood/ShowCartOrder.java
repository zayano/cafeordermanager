package com.example.administrator.cafeom.ListMenuFood;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.administrator.cafeom.Helper.CustomAdapter;
import com.example.administrator.cafeom.Helper.DBHelperCart;
import com.example.administrator.cafeom.Helper.ItemsModel;
import com.example.administrator.cafeom.OrderModel;
import com.example.administrator.cafeom.R;

import java.util.ArrayList;

public class ShowCartOrder extends AppCompatActivity {

    ListView list;
    CustomAdapter customAdapter;
    ArrayList<ItemsModel> arrayList;
    DBHelperCart dbHelperCart;
    ItemsModel item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_cart_order);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        list = findViewById(R.id.listOfItems);
        dbHelperCart = new DBHelperCart(this);
        Intent intent = getIntent();
        final String userloged = intent.getStringExtra("name");

        arrayList = new ArrayList<>();
        arrayList = dbHelperCart.getAllItemsCustomers();

        customAdapter = new CustomAdapter(this,arrayList);
        list.setAdapter(customAdapter);


        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Toast.makeText(getApplicationContext(),"Coming Soon",Toast.LENGTH_LONG).show();
//                item = customAdapter.getItem(i);
//                Intent intent = new Intent(ShowAllCustomers.this,ProductView.class);
//
//                intent.putExtra("UnitCode",item.getUnitCode());
//                intent.putExtra("ItemName",item.getItemname());
//                intent.putExtra("Price",item.getPrice());
//                intent.putExtra("Discription",item.getDiscription());
//                intent.putExtra("Available",item.getAvailable());
//                intent.putExtra("Seller",item.getSeller());
//                startActivity(intent);

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }
}
