package com.example.administrator.cafeom;

public class OrderModel {
    public String orderID;
    private String nameOrder;
    private String costOrder;
    private String quantity;
    private String total;


    public String getTotal() {
        return total;
    }

    public String getOrderID(){
        return orderID;
    }

    public String getCostOrder(){
        return costOrder;
    }

    public String getQuantity(){
        return quantity;
    }

    public String getNameOrder() {
        return nameOrder;
    }

    public void setOrderID(String orderID){
        this.orderID = orderID;
    }

    public void setNameOrder(String nameOrder){
        this.nameOrder = nameOrder;
    }

    public void setCostOrder(String costOrder){
        this.costOrder = costOrder;
    }

    public void setQuantity(String quantity){
        this.quantity = quantity;
    }

    public void setTotal(String quantity){
        this.quantity = total;
    }
}
